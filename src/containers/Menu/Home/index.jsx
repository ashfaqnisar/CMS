import {Col, Container, Row} from "reactstrap";
import React from "react";

const Home = () => (
    <Container className="dashboard">
        <Row>
            <Col md={12}>
                <h3 className="page-title">Home</h3>
            </Col>
        </Row>
    </Container>
);

export default (Home);