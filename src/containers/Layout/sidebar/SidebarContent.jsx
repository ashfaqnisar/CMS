import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SidebarLink from './SidebarLink';

class SidebarContent extends Component {
    static propTypes = {
        changeToDark: PropTypes.func.isRequired,
        changeToLight: PropTypes.func.isRequired,
        onClick: PropTypes.func.isRequired,
    };

    hideSidebar = () => {
        const {onClick} = this.props;
        onClick();
    };

    render() {

        return (
            <div className="sidebar__content">
                <ul className="sidebar__block">
                    <SidebarLink title="Home" icon="home" route="/home"
                                 onClick={this.hideSidebar}/>
                    <SidebarLink
                        title="Student"
                        icon="store"
                        route="/student"
                        onClick={this.hideSidebar}
                    />
                    <SidebarLink
                        title="Staff"
                        icon="heart-pulse"
                        route="/staff"
                        onClick={this.hideSidebar}
                    />
                    <SidebarLink
                        title="Department"
                        icon="rocket"
                        route="/department"
                        onClick={this.hideSidebar}
                    />
                    <SidebarLink
                        title="Fee"
                        icon="text-align-justify"
                        route="/fee"
                        onClick={this.hideSidebar}
                    />
                </ul>
                <ul className="sidebar__block">
                    <SidebarLink title="Log Out" icon="exit" route="/login"/>
                </ul>

            </div>
        );
    }
}

export default SidebarContent;
