import React from 'react';
import LogInForm from './components/LogInForm';

const logo = `${process.env.PUBLIC_URL}/img/Ezerka.png`;

const LogIn = () => (
    <div className="account">
        <div className="account__wrapper">
            <div className="account__card">
                <div className="account__head">
                    <div className="account__title text-center">
                        <img style={{align: "center", width: "50%",}} src={logo} alt="Logo"/>
                    </div>
                </div>
                <LogInForm onSubmit/>
            </div>
        </div>
    </div>
);

export default LogIn;
