import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import Layout from '../../../Layout/index';


import Home from '../../../Menu/Home/index'
import Student from '../../../Menu/Student/index'
import Staff from '../../../Menu/Staff/index'
import Department from '../../../Menu/Department/index'
import Fee from '../../../Menu/Fee/index'


const WrappedRoutes = () => (
    <div>
        <Layout/>
        <div className="container__wrap">
            <Switch>
                <Route path={"/Home"} component={Home}/>
                <Route path={"/Student"} component={Student}/>
                <Route path={"/Department"} component={Department}/>
                <Route path={"/Fee"} component={Fee}/>
                <Route path={"/Staff"} component={Staff}/>
                <Redirect from={"*"} to={"/404"}/>
            </Switch>

        </div>
    </div>
);

export default (WrappedRoutes);
