import React from 'react';
import {Route, Switch} from 'react-router-dom';
import MainWrapper from '../MainWrapper';
import LogIn from '../../Account/LogIn/index'
import NotFound404 from '../../DefaultPage/404/index';
import LockScreen from '../../Account/LockScreen/index';
import Register from '../../Account/Register/index';
import RegisterPhoto from '../../Account/RegisterPhoto/index';
import WrappedRoutes from './WrappedRoutes';


const Router = () => (
    <MainWrapper>
        <main>
            <Switch>
                <Route exact path="/" component={LogIn}/>
                <Route path="/lock_screen" component={LockScreen}/>
                <Route path="/login" component={LogIn}/>
                <Route path="/register" component={Register}/>
                <Route path="/register_photo" component={RegisterPhoto}/>
                <Route path="/404" component={NotFound404}/>
                <Route path="/" component={WrappedRoutes}/>
            </Switch>
        </main>
    </MainWrapper>
);

export default Router;
